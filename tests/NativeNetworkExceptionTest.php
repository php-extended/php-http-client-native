<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-native library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\NativeNetworkException;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;

/**
 * NativeNetworkExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\NativeNetworkException
 *
 * @internal
 *
 * @small
 */
class NativeNetworkExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var NativeNetworkException
	 */
	protected NativeNetworkException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(NativeNetworkException::class, $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new NativeNetworkException(
			$this->getMockForAbstractClass(RequestInterface::class),
			'message',
			-1,
			null,
		);
	}
	
}

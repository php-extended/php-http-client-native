<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-native library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Certificate\CertificateProviderInterface;
use PhpExtended\HttpClient\NativeClient;
use PhpExtended\HttpClient\NativeOptions;
use PhpExtended\HttpClient\NativeOptionsFactory;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;

/**
 * NativeClientTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\NativeClient
 *
 * @internal
 *
 * @small
 */
class NativeClientTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var NativeClient
	 */
	protected NativeClient $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new NativeClient(
			$this->getMockForAbstractClass(ResponseFactoryInterface::class),
			$this->getMockForAbstractClass(StreamFactoryInterface::class),
			new NativeOptionsFactory(),
			new NativeOptions(),
			$this->getMockForAbstractClass(CertificateProviderInterface::class),
		);
	}
	
}

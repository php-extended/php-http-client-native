<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-native library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Stringable;

/**
 * NativeSslOptions class file.
 * 
 * This class represents all the options that are used for the ssl and tls 
 * stream wrappers. This stream wrapper is also used within the https and ftps 
 * streams.
 * 
 * @author Anastaszor
 * @see https://secure.php.net/manual/en/context.ssl.php
 * @SuppressWarnings("PHPMD.TooManyFields")
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class NativeSslOptions implements Stringable
{
	
	/**
	 * Peer name to be used. If this value is not set, then the name is guessed
	 * based on the hostname used when opening the stream.
	 * 
	 * @var ?string
	 */
	protected ?string $_peerName = null;
	
	/**
	 * Require verification of SSL certificate used. Defaults to TRUE.
	 * 
	 * @var boolean
	 */
	protected bool $_verifyPeer = true;
	
	/**
	 * Require verification of peer name. Defaults to TRUE.
	 * 
	 * @var boolean
	 */
	protected bool $_verifyPeerName = true;
	
	/**
	 * Allow self-signed certificates. Requires verify_peer. Defaults to FALSE.
	 * 
	 * @var boolean
	 */
	protected bool $_allowSelfSigned = false;
	
	/**
	 * Location of Certificate Authority file on local filesystem which should
	 * be used with the verify_peer context option to authenticate the identity
	 * of the remote peer. 
	 * 
	 * @var ?string
	 */
	protected ?string $_cafile = null;
	
	/**
	 * If cafile is not specified or if the certificate is not found there, the
	 * directory pointed to by capath is searched for a suitable certificate.
	 * capath must be a correctly hashed certificate directory.
	 * 
	 * @var ?string
	 */
	protected ?string $_capath = null;
	
	/**
	 * Path to local certificate file on filesystem. It must be a PEM encoded
	 * file which contains your certificate and protected key. It can optionally
	 * contain the certificate chain of issuers. The protected key also may be
	 * contained in a separate file specified by local_pk. 
	 * 
	 * @var ?string
	 */
	protected ?string $_localCert = null;
	
	/**
	 * Path to local protected key file on filesystem in case of separate files
	 * for certificate (local_cert) and protected key.
	 * 
	 * @var ?string
	 */
	protected ?string $_localPk = null;
	
	/**
	 * Passphrase with which your local_cert file was encoded.
	 * 
	 * @var ?string
	 */
	protected ?string $_passphrase = null;
	
	/**
	 * Abort if the certificate chain is too deep.
	 * 
	 * @var integer
	 */
	protected int $_verifyDepths = 5;
	
	/**
	 * Sets the list of available ciphers. The format of the string is
	 * described in » ciphers(1). Defaults to DEFAULT.
	 * 
	 * @var ?string
	 * @see https://www.openssl.org/docs/manmaster/man1/ciphers.html#CIPHER-LIST-FORMAT
	 */
	protected ?string $_ciphers = null;
	
	/**
	 * If set to TRUE a peer_certificate context option will be created
	 * containing the peer certificate. 
	 * 
	 * @var boolean
	 */
	protected bool $_capturePeerCert = true;
	
	/**
	 * If set to TRUE a peer_certificate_chain context option will be created
	 * containing the certificate chain.
	 * 
	 * @var boolean
	 */
	protected bool $_capturePeerCertChain = true;
	
	/**
	 * If set to TRUE server name indication will be enabled. Enabling SNI
	 * allows multiple certificates on the same IP address.
	 * 
	 * @var boolean
	 */
	protected bool $_sniEnabled = true;
	
	/**
	 * If set, disable TLS compression. This can help mitigate the CRIME
	 * attack vector. 
	 * 
	 * @var boolean
	 */
	protected bool $_disableCompression = true;
	
	/**
	 * Aborts when the remote certificate digest doesn't match the specified
	 * hash. When a string is used, the length will determine which hashing
	 * algorithm is applied, either "md5" (32) or "sha1" (40). When an array
	 * is used, the keys indicate the hashing algorithm name and each
	 * corresponding value is the expected digest.
	 * 
	 * @var ?string
	 */
	protected ?string $_peerFingerprint = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the array that will be used to build the context.
	 *
	 * @return array<string, boolean|integer|float|string>
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function toArray() : array
	{
		$opts = [
			'allow_self_signed' => $this->_allowSelfSigned,
			'capture_peer_cert' => $this->_capturePeerCert,
			'capture_peer_cert_chain' => $this->_capturePeerCertChain,
			'disable_compression' => $this->_disableCompression,
			'sni_enabled' => $this->_sniEnabled,
			'verify_depths' => $this->_verifyDepths,
			'verify_peer' => $this->_verifyPeer,
			'verify_peer_name' => $this->_verifyPeerName,
		];
		
		if(null !== $this->_peerName && '' !== $this->_peerName)
		{
			$opts['peer_name'] = $this->_peerName;
		}
		
		if(null !== $this->_cafile && '' !== $this->_cafile)
		{
			$opts['cafile'] = $this->_cafile;
		}
		
		if(null !== $this->_capath && '' !== $this->_capath)
		{
			$opts['capath'] = $this->_capath;
		}
		
		if(null !== $this->_localCert && '' !== $this->_localCert)
		{
			$opts['local_cert'] = $this->_localCert;
		}
		
		if(null !== $this->_localPk && '' !== $this->_localPk)
		{
			$opts['local_pk'] = $this->_localPk;
		}
		
		if(null !== $this->_passphrase && '' !== $this->_passphrase)
		{
			$opts['passphrase'] = $this->_passphrase;
		}
		
		if(null !== $this->_ciphers && '' !== $this->_ciphers)
		{
			$opts['ciphers'] = $this->_ciphers;
		}
		
		if(null !== $this->_peerFingerprint && '' !== $this->_peerFingerprint)
		{
			$opts['peer_fingerprint'] = $this->_peerFingerprint;
		}
		
		return $opts;
	}
	
	/**
	 * Gets the name of the peer to be used.
	 * 
	 * @return ?string
	 */
	public function getPeerName() : ?string
	{
		return $this->_peerName;
	}
	
	/**
	 * Gets whether to verify the peer ssl certificate.
	 * 
	 * @return boolean
	 */
	public function shouldVerifyPeer() : bool
	{
		return $this->_verifyPeer;
	}
	
	/**
	 * Gets whether to verify the peer name.
	 * 
	 * @return boolean
	 */
	public function shouldVerifyPeerName() : bool
	{
		return $this->_verifyPeerName;
	}
	
	/**
	 * Gets whether to allow self-signed certificate.
	 * 
	 * @return boolean
	 */
	public function shouldAllowSelfSigned() : bool
	{
		return $this->_allowSelfSigned;
	}
	
	/**
	 * Gets the location of the Certificate Authority file on the local
	 * filesystem, which will be used to verify the peer name and certificate.
	 * 
	 * @return ?string
	 */
	public function getCafile() : ?string
	{
		return $this->_cafile;
	}
	
	/**
	 * Gets the location of a directory when to search for the Certificate
	 * Autority when cafile is not provided.
	 * 
	 * @return ?string
	 */
	public function getCapath() : ?string
	{
		return $this->_capath;
	}
	
	/**
	 * Path to local PEM encoded certificate.
	 * 
	 * @return ?string
	 */
	public function getLocalCert() : ?string
	{
		return $this->_localCert;
	}
	
	/**
	 * Path to local PEM encoded protected key.
	 * 
	 * @return ?string
	 */
	public function getLocalPk() : ?string
	{
		return $this->_localPk;
	}
	
	/**
	 * Gets the passphrase that was used to encode the local certificate file
	 * was encoded.
	 * 
	 * @return ?string
	 */
	public function getPassphrase() : ?string
	{
		return $this->_passphrase;
	}
	
	/**
	 * Gets the maximum depths to verify. Aborts if the depths of the
	 * certificate chain is too much.
	 * 
	 * @return integer
	 */
	public function getVerifyDepths() : int
	{
		return $this->_verifyDepths;
	}
	
	/**
	 * Gets the allowed ciphers to use.
	 * 
	 * @return ?string
	 */
	public function getCiphers() : ?string
	{
		return $this->_ciphers;
	}
	
	/**
	 * Gets whether to capture the peer certificate.
	 * 
	 * @return boolean
	 */
	public function shouldCapturePeerCert() : bool
	{
		return $this->_capturePeerCert;
	}
	
	/**
	 * Gets whether to capture the peer certificate chain.
	 * 
	 * @return boolean
	 */
	public function shouldCapturePeerCertChain() : bool
	{
		return $this->_capturePeerCertChain;
	}
	
	/**
	 * Gets whether to allow multiple certificates on this server.
	 * 
	 * @return boolean
	 */
	public function hasSniEnabled() : bool
	{
		return $this->_sniEnabled;
	}
	
	/**
	 * Whether to disable the compression of the ssl flux.
	 * 
	 * @return boolean
	 */
	public function hasDisabledCompression() : bool
	{
		return $this->_disableCompression;
	}
	
	/**
	 * The expected fingerprint of the peer.
	 * 
	 * @return ?string
	 */
	public function getPeerFingerprint() : ?string
	{
		return $this->_peerFingerprint;
	}
	
	/**
	 * Sets the name of the peer to check from.
	 * 
	 * @param string $peerName
	 */
	public function setPeerName(string $peerName) : void
	{
		$this->_peerName = $peerName;
	}
	
	/**
	 * Sets whether to verify the peer certificate.
	 * 
	 * @param boolean $verifyPeer
	 */
	public function setVerifyPeer(bool $verifyPeer) : void
	{
		$this->_verifyPeer = $verifyPeer;
	}
	
	/**
	 * Sets whether to verify the peer name.
	 * 
	 * @param boolean $verifyPeerName
	 */
	public function setVerifyPeerName(bool $verifyPeerName) : void
	{
		$this->_verifyPeerName = $verifyPeerName;
	}
	
	/**
	 * Sets whether to allow self signed certificates.
	 * 
	 * @param boolean $allowSelfSigned
	 */
	public function setAllowSelfSigned(bool $allowSelfSigned) : void
	{
		$this->_allowSelfSigned = $allowSelfSigned;
	}
	
	/**
	 * The path to the file containing the certificate authorities to trust.
	 * 
	 * @param string $cafile
	 */
	public function setCafile(string $cafile) : void
	{
		$this->_cafile = $cafile;
	}
	
	/**
	 * The path of the directory containing the Cerfiticate Authority file.
	 * 
	 * @param string $capath
	 */
	public function setCapath(string $capath) : void
	{
		$this->_capath = $capath;
	}
	
	/**
	 * Sets the path to the local certificate (PEM encoded).
	 * 
	 * @param string $localCert
	 */
	public function setLocalCert(string $localCert) : void
	{
		$this->_localCert = $localCert;
	}
	
	/**
	 * Sets the path to the local protected key (PEM encoded).
	 * 
	 * @param string $localPk
	 */
	public function setLocalPk(string $localPk) : void
	{
		$this->_localPk = $localPk;
	}
	
	/**
	 * Sets the passphrase of the local certificate file.
	 * 
	 * @param string $passphrase
	 */
	public function setPassphrase(string $passphrase) : void
	{
		$this->_passphrase = $passphrase;
	}
	
	/**
	 * Sets the maximum depths to verify the certificate chain.
	 * 
	 * @param integer $verifyDepths
	 */
	public function setVerifyDepths(int $verifyDepths) : void
	{
		$this->_verifyDepths = $verifyDepths;
	}
	
	/**
	 * Sets the list of allowed ciphers.
	 * 
	 * @param string $ciphers
	 */
	public function setCiphers(string $ciphers) : void
	{
		$this->_ciphers = $ciphers;
	}
	
	/**
	 * Sets whether to capture the peer certificate.
	 * 
	 * @param boolean $capturePeerCert
	 */
	public function setCapturePeerCert(bool $capturePeerCert) : void
	{
		$this->_capturePeerCert = $capturePeerCert;
	}
	
	/**
	 * Sets whether to capture the peer certificate chain.
	 * 
	 * @param boolean $capturePeerCertChain
	 */
	public function setCapturePeerCertChain(bool $capturePeerCertChain) : void
	{
		$this->_capturePeerCertChain = $capturePeerCertChain;
	}
	
	/**
	 * Sets whether to enable multiple certificates on this server.
	 * 
	 * @param boolean $sniEnabled
	 */
	public function setSniEnabled(bool $sniEnabled) : void
	{
		$this->_sniEnabled = $sniEnabled;
	}
	
	/**
	 * Sets whether to disable the compression to mitigate CRIME attacks.
	 * 
	 * @param boolean $disableCompression
	 */
	public function setDisableCompression(bool $disableCompression) : void
	{
		$this->_disableCompression = $disableCompression;
	}
	
	/**
	 * Sets the expected peer fingerprint.
	 * 
	 * @param string $peerFingerprint
	 */
	public function setPeerFingerprint(string $peerFingerprint) : void
	{
		$this->_peerFingerprint = $peerFingerprint;
	}
	
	/**
	 * Merges this stream options with the given other options. This method
	 * does not modifies the current options object and creates a new object
	 * with the merged properties of both objects.
	 *
	 * When merging, if two properties are defined within this object and the
	 * other, the properties of the other object will override the properties
	 * within current object.
	 *
	 * @param ?NativeSslOptions $other
	 * @return NativeSslOptions
	 */
	public function mergeWith(?NativeSslOptions $other) : NativeSslOptions
	{
		if(null === $other)
		{
			return $this;
		}
		
		$newobj = new self();
		$newobj->_peerName = $other->getPeerName() ?? $this->getPeerName();
		$newobj->_verifyPeer = $other->shouldVerifyPeer() && $this->shouldVerifyPeer();
		$newobj->_verifyPeerName = $other->shouldVerifyPeerName() && $this->shouldVerifyPeerName();
		$newobj->_allowSelfSigned = $other->shouldAllowSelfSigned() || $this->shouldAllowSelfSigned();
		$newobj->_cafile = $other->getCafile() ?? $this->getCafile();
		$newobj->_capath = $other->getCapath() ?? $this->getCapath();
		$newobj->_localCert = $other->getLocalCert() ?? $this->getLocalCert();
		$newobj->_passphrase = $other->getPassphrase() ?? $this->getPassphrase();
		$newobj->_localPk = $other->getLocalPk() ?? $this->getLocalPk();
		$newobj->_verifyDepths = \max($other->getVerifyDepths(), $this->getVerifyDepths());
		$newobj->_ciphers = $other->getCiphers() ?? $this->getCiphers();
		$newobj->_capturePeerCert = $other->shouldCapturePeerCert() && $this->shouldCapturePeerCert();
		$newobj->_capturePeerCertChain = $other->shouldCapturePeerCertChain() && $this->shouldCapturePeerCertChain();
		$newobj->_sniEnabled = $other->hasSniEnabled() && $this->hasSniEnabled();
		$newobj->_disableCompression = $other->hasDisabledCompression() && $this->hasDisabledCompression();
		$newobj->_peerFingerprint = $other->getPeerFingerprint() ?? $this->getPeerFingerprint();
		
		return $newobj;
	}
	
}

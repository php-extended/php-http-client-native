<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-native library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Stringable;

/**
 * NativeCurlOptions class file.
 * 
 * This class represents all the options that are used for the curl stream
 * wrapper.
 * 
 * @author Anastaszor
 * @see https://secure.php.net/manual/en/context.curl.php
 */
class NativeCurlOptions implements Stringable
{
	
	/**
	 * GET, POST, or any other HTTP method supported by the remote server. 
	 * Defaults to GET.
	 * 
	 * @var string
	 */
	protected string $_method = 'GET';
	
	/**
	 * Additional headers to be sent during request. Values in this option will
	 * override other values (such as User-agent:, Host:, and Authentication:). 
	 * 
	 * @var array<string, array<integer, string>>
	 */
	protected array $_headers = [];
	
	/**
	 * Value to send with User-Agent: header. By default the user_agent php.ini
	 * setting is used.
	 * 
	 * @var ?string
	 */
	protected ?string $_userAgent = null;
	
	/**
	 * Additional data to be sent after the headers. This option is not used
	 * for GET or HEAD requests.
	 * 
	 * @var ?string
	 */
	protected ?string $_content = null;
	
	/**
	 * URI specifying address of proxy server. (e.g. tcp://proxy.example.com:5100).
	 * 
	 * @var ?string
	 */
	protected ?string $_proxy = null;
	
	/**
	 * The max number of redirects to follow. Value 1 or less means that no
	 * redirects are followed. Defaults to 20.
	 * 
	 * @var integer
	 */
	protected int $_maxRedirects = 20;
	
	/**
	 * Verify the host. This option is available for both the http and ftp
	 * protocol wrappers.
	 * 
	 * @var boolean
	 */
	protected bool $_curlVerifySslHost = true;
	
	/**
	 * Require verification of SSL certificate used. This option is available
	 * for both the http and ftp protocol wrappers.
	 * 
	 * @var boolean
	 */
	protected bool $_curlVerifySslPeer = true;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the array that will be used to build the context.
	 *
	 * @return array<string, boolean|integer|string>
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function toArray() : array
	{
		$strheaders = [];
		$cookieh = [];
		
		// handle all other headers
		foreach($this->_headers as $key => $headers)
		{
			if('Cookie' === $key)
			{
				foreach($headers as $header)
				{
					$cookieh[] = $header;
				}
				
				continue;
			}
			
			foreach($headers as $header)
			{
				$strheaders[] = $key.': '.$header;
			}
		}
		
		if(\count($cookieh) > 0)
		{
			// handle cookie concatenation and unicity
			$strheaders[] = 'Cookie: '.\implode('; ', $cookieh);
		}
		
		$opts = [
			'curl_verify_ssl_host' => $this->_curlVerifySslHost,
			'curl_verify_ssl_peer' => $this->_curlVerifySslPeer,
			'max_redirects' => $this->_maxRedirects,
			'method' => $this->_method,
		];
		
		if(\count($strheaders) > 0)
		{
			$opts['header'] = \implode("\r\n", $strheaders)."\r\n";
		}
		
		if(null !== $this->_userAgent && '' !== $this->_content)
		{
			$opts['user_agent'] = $this->_userAgent;
		}
		
		if(null !== $this->_content && '' !== $this->_content)
		{
			$opts['content'] = $this->_content;
		}
		
		if(null !== $this->_proxy && '' !== $this->_proxy)
		{
			$opts['proxy'] = $this->_proxy;
		}
		
		return $opts;
	}
	
	/**
	 * Gets the http verb that will be used.
	 * 
	 * @return string
	 */
	public function getMethod() : string
	{
		return $this->_method;
	}
	
	/**
	 * Gets the headers that will be sent.
	 * 
	 * @return array<string, array<integer, string>>
	 */
	public function getHeaders() : array
	{
		return $this->_headers;
	}
	
	/**
	 * Gets the user agent string that will be used.
	 * 
	 * @return ?string
	 */
	public function getUserAgent() : ?string
	{
		return $this->_userAgent;
	}
	
	/**
	 * Gets the raw contents that will be used.
	 * 
	 * @return ?string
	 */
	public function getContent() : ?string
	{
		return $this->_content;
	}
	
	/**
	 * Gets the proxy address and port that will be used.
	 * 
	 * @return ?string
	 */
	public function getProxy() : ?string
	{
		return $this->_proxy;
	}
	
	/**
	 * Gets the maximum number of redirects.
	 * 
	 * @return integer
	 */
	public function getMaxRedirects() : int
	{
		return $this->_maxRedirects;
	}
	
	/**
	 * Gets whether to verify the ssl host certificate.
	 * 
	 * @return boolean
	 */
	public function shouldCurlVerifySslHost() : bool
	{
		return $this->_curlVerifySslHost;
	}
	
	/**
	 * Gets whether to verify the ssl peer certificate.
	 * 
	 * @return boolean
	 */
	public function shouldCurlVerifySslPeer() : bool
	{
		return $this->_curlVerifySslPeer;
	}
	
	/**
	 * Sets the http verb to use.
	 * 
	 * @param string $method
	 */
	public function setMethod(string $method) : void
	{
		$this->_method = $method;
	}
	
	/**
	 * Adds all the additional headers given in the array. This array values 
	 * may be strings, or an array of strings, which represents different
	 * values for the same header. All values will be appened to the already
	 * present values of the headers.
	 * 
	 * @param array<string, string|array<int, string>> $headers
	 */
	public function setHeaders(array $headers) : void
	{
		$this->_headers = [];
		
		foreach($headers as $headerName => $headerValues)
		{
			if(\is_string($headerValues))
			{
				$this->addHeader($headerName, $headerValues);
			}
			
			if(\is_array($headerValues))
			{
				foreach($headerValues as $headerValue)
				{
					$this->addHeader($headerName, $headerValue);
				}
			}
		}
	}
	
	/**
	 * Adds a specific header to the header list.
	 * 
	 * @param string $headerName
	 * @param string $headerValue
	 */
	public function addHeader(string $headerName, string $headerValue) : void
	{
		$this->_headers[$headerName][] = $headerValue;
	}
	
	/**
	 * Sets the user agent string to use.
	 * 
	 * @param string $userAgent
	 */
	public function setUserAgent(string $userAgent) : void
	{
		$this->_userAgent = $userAgent;
	}
	
	/**
	 * Sets the contents of the request.
	 * 
	 * @param string $content
	 */
	public function setContent(string $content) : void
	{
		$this->_content = $content;
	}
	
	/**
	 * Sets the proxy address and port to use.
	 * 
	 * @param string $proxy
	 */
	public function setProxy(string $proxy) : void
	{
		$this->_proxy = $proxy;
	}
	
	/**
	 * Sets the maximum number of redirects.
	 * 
	 * @param integer $maxRedirects
	 */
	public function setMaxRedirects(int $maxRedirects) : void
	{
		$this->_maxRedirects = $maxRedirects;
	}
	
	/**
	 * Sets whether to verify the host ssl certificate.
	 * 
	 * @param boolean $curlVerifySslHost
	 */
	public function setCurlVerifySslHost(bool $curlVerifySslHost) : void
	{
		$this->_curlVerifySslHost = $curlVerifySslHost;
	}
	
	/**
	 * Sets whether to verify the peer ssl certificates. 
	 * 
	 * @param boolean $curlVerifySslPeer
	 */
	public function setCurlVerifySslPeer(bool $curlVerifySslPeer) : void
	{
		$this->_curlVerifySslPeer = $curlVerifySslPeer;
	}
	
	/**
	 * Merges this stream options with the given other options. This method
	 * does not modifies the current options object and creates a new object
	 * with the merged properties of both objects.
	 *
	 * When merging, if two properties are defined within this object and the
	 * other, the properties of the other object will override the properties
	 * within current object.
	 *
	 * @param ?NativeCurlOptions $other
	 * @return NativeCurlOptions
	 */
	public function mergeWith(?NativeCurlOptions $other) : NativeCurlOptions
	{
		if(null === $other)
		{
			return $this;
		}
		
		$newobj = new self();
		$newobj->_method = $other->getMethod();
		
		foreach($this->getHeaders() as $hkey => $hhs)
		{
			foreach($hhs as $header)
			{
				$newobj->_headers[(string) $hkey][] = $header;
			}
		}
		
		foreach($other->getHeaders() as $hkey => $hhs)
		{
			foreach($hhs as $header)
			{
				$newobj->_headers[(string) $hkey][] = $header;
			}
		}
		
		$newobj->_userAgent = $other->getUserAgent() ?? $this->getUserAgent();
		$newobj->_content = $other->getContent() ?? $this->getContent();
		$newobj->_proxy = $other->getProxy() ?? $this->getProxy();
		$newobj->_maxRedirects = \min($other->getMaxRedirects(), $this->getMaxRedirects());
		$newobj->_curlVerifySslHost = $other->shouldCurlVerifySslHost() || $this->shouldCurlVerifySslHost();
		$newobj->_curlVerifySslPeer = $other->shouldCurlVerifySslPeer() || $this->shouldCurlVerifySslPeer();
		
		return $newobj;
	}
	
}

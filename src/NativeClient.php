<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-native library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use InvalidArgumentException;
use PhpExtended\Certificate\CertificateProviderInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use RuntimeException;
use Stringable;

/**
 * NativeClient class file.
 * 
 * This class is a simple implementation of the ProcessorInterface.
 * 
 * @author Anastaszor
 */
class NativeClient implements ClientInterface, Stringable
{
	
	/**
	 * The response factory.
	 * 
	 * @var ResponseFactoryInterface
	 */
	protected ResponseFactoryInterface $_responseFactory;
	
	/**
	 * The stream factory.
	 * 
	 * @var StreamFactoryInterface
	 */
	protected StreamFactoryInterface $_streamFactory;
	
	/**
	 * The native stream options factory.
	 * 
	 * @var NativeOptionsFactory
	 */
	protected NativeOptionsFactory $_streamOptionsFactory;
	
	/**
	 * Additional options (option name => option value) to add to the context
	 * of every request that is processed by this processor.
	 * 
	 * @var ?NativeOptions
	 */
	protected ?NativeOptions $_defaultOptions = null;
	
	/**
	 * The certificate provider.
	 *
	 * @var ?CertificateProviderInterface
	 */
	protected ?CertificateProviderInterface $_provider = null;
	
	/**
	 * Builds a new NativeClient with the given configuration.
	 * 
	 * @param ResponseFactoryInterface $responseFactory
	 * @param StreamFactoryInterface $streamFactory
	 * @param NativeOptionsFactory $optionsFactory
	 * @param ?NativeOptions $defaultOptions
	 * @param ?CertificateProviderInterface $provider
	 */
	public function __construct(
		ResponseFactoryInterface $responseFactory,
		StreamFactoryInterface $streamFactory,
		NativeOptionsFactory $optionsFactory,
		?NativeOptions $defaultOptions = null,
		?CertificateProviderInterface $provider = null
	) {
		$this->_responseFactory = $responseFactory;
		$this->_streamFactory = $streamFactory;
		$this->_streamOptionsFactory = $optionsFactory;
		$this->_defaultOptions = $defaultOptions;
		$this->_provider = $provider;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Client\ClientInterface::sendRequest()
	 * @SuppressWarnings("PHPMD.ErrorControlOperator")
	 */
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		$options = $this->_streamOptionsFactory->createFromRequest($request);
		if(null !== $this->_provider)
		{
			$capath = $this->_provider->getCertificateFilePath();
			if(null === $options->getSslOptions()->getCafile())
			{
				$options->getSslOptions()->setCafile($capath);
			}
			if(null === $options->getSslOptions()->getCapath())
			{
				$options->getSslOptions()->setCapath($capath);
			}
		}
		if(null !== $this->_defaultOptions)
		{
			$options = $this->_defaultOptions->mergeWith($options);
		}
		
		$context = \stream_context_create($options->toArray());
		$options->applyParamsToContext($context);
		
		// remove the fragment part of the uri (may cause http 400 bad request)
		$url = $request->getUri()->withFragment('')->__toString();
		
		// $http_response_header should be automagically filled
		// https://secure.php.net/manual/en/reserved.variables.httpresponseheader.php
		$http_response_header = [];
		
		// https://stackoverflow.com/questions/272361/how-can-i-handle-the-warning-of-file-get-contents-function-in-php#272377
		// an error handler with return false should be used to have error_get_last() to work correctly
		// and we need to restore the previous error handler after getting contents
		/* {{{ */ \set_error_handler(function()
		{
		/* ||| */ return false;
		});
		/* ||| */ \error_clear_last();	// just in case
		$downloadFilePath = $request->getHeaderLine('X-Php-Download-File');
		if(!empty($downloadFilePath) && \is_dir(\dirname($downloadFilePath)))
		{
			$data = @\copy($url, $downloadFilePath, $context);
			$data = false === $data ? null : ''; // if data is true, so it is a success
			/* ||| */ $error = \error_get_last() ?? [];
			/* }}} */ \restore_error_handler();
			
			return $this->buildResponse($request, $http_response_header, $error, $data, $downloadFilePath);
		}
		
		$data = @\file_get_contents($url, false, $context);
		$data = false === $data ? null : $data;
		/* ||| */ $error = \error_get_last() ?? [];
		/* }}} */ \restore_error_handler();
		
		return $this->buildResponse($request, $http_response_header, $error, $data, $downloadFilePath);
	}
	
	/**
	 * Builds the response from the status of the returned data from the http
	 * request.
	 * 
	 * @param RequestInterface $request
	 * @param array<int, string> $httpResponseHeaders
	 * @param array<string, int|string> $error
	 * @param ?string $data
	 * @param string $downloadFilePath
	 * @return ResponseInterface
	 * @throws NativeNetworkException
	 */
	public function buildResponse(RequestInterface $request, array $httpResponseHeaders, array $error, ?string $data, string $downloadFilePath) : ResponseInterface
	{
		if(null !== $data)
		{
			$response = $this->createFromHeaders($httpResponseHeaders);
			$body = $this->createBody($request, $data, $downloadFilePath);
			
			try
			{
				$response = $response->withBody($body);
			}
			catch(InvalidArgumentException $e)
			{
				// nothing to do
			}
			
			try
			{
				// needed to get back the information to which was the target
				$response = $response->withAddedHeader('X-Request-Method', $request->getMethod());
				$response = $response->withAddedHeader('X-Request-Protocol', $request->getProtocolVersion());
				$response = $response->withAddedHeader('X-Request-Target', $request->getRequestTarget());
				$response = $response->withAddedHeader('X-Request-Uri', $request->getUri()->__toString());
				
				foreach($request->getHeaders() as $key => $headerValues)
				{
					$response = $response->withAddedHeader('X-Request-Header-'.((string) $key), $headerValues);
				}
			}
			catch(InvalidArgumentException $e)
			{
				// nothing to add
			}
			
			return $response;
		}
		
		$errormsg = '(Unknown Error not catched by error_get_last())';
		if(isset($error['message']))
		{
			$errormsg = $error['message'];
		}
		$message = 'Failed to load url "{url}" : {message}';
		$context = ['{url}' => $request->getUri()->__toString(), '{message}' => $errormsg];
		
		throw new NativeNetworkException($request, \strtr($message, $context));
	}
	
	/**
	 * Create a new response from the raw data from the function.
	 * 
	 * @param array<integer|string, null|boolean|integer|float|string> $headers
	 * @return ResponseInterface
	 */
	public function createFromHeaders(array $headers) : ResponseInterface
	{
		$response = $this->_responseFactory->createResponse();
		
		foreach($headers as $value)
		{
			$value = (string) $value;
			$pos = \mb_strpos($value, ':');
			if(false !== $pos)
			{
				try
				{
					$response = $response->withAddedHeader(
						\trim((string) \mb_substr($value, 0, $pos)),
						\trim((string) \mb_substr($value, $pos + 1)),
					);
				}
				catch(InvalidArgumentException $exc)
				{
					// nothing to add
				}
				continue;
			}
			
			if(0 === \mb_strpos($value, 'HTTP/'))
			{
				$matches = [];
				if(\preg_match('#HTTP/(\\d(\\.\\d)?) (\\d+) (.*)$#', $value, $matches))
				{
					/** @phpstan-ignore-next-line */
					if(isset($matches[1], $matches[3], $matches[4]))
					{
						$response = $response->withProtocolVersion($matches[1]);
						
						try
						{
							$response = $response->withStatus((int) $matches[3], $matches[4]);
						}
						catch(InvalidArgumentException $exc)
						{
							// nothing to do
						}
					}
				}
			}
		}
		
		return $response;
	}
	
	/**
	 * Creates a suitable body for the response.
	 * 
	 * @param RequestInterface $request
	 * @param ?string $data
	 * @param string $downloadFilePath
	 * @return \Psr\Http\Message\StreamInterface
	 * @throws NativeNetworkException
	 */
	public function createBody(RequestInterface $request, ?string $data, string $downloadFilePath)
	{
		if(!empty($downloadFilePath) && \is_file($downloadFilePath))
		{
			try
			{
				return $this->_streamFactory->createStreamFromFile($downloadFilePath);
			}
			catch(InvalidArgumentException|RuntimeException $e)
			{
				$message = 'Failed to open file at {path} : {msg}';
				$context = ['{path}' => $downloadFilePath, '{msg}' => $e->getMessage()];
				
				throw new NativeNetworkException($request, \strtr($message, $context), 2, $e);
			}
		}
		
		return $this->_streamFactory->createStream((string) $data);
	}
	
}

<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-native library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Stringable;

/**
 * NativeOptions class file.
 * 
 * This class gathers all the options from all the different possible contexts.
 * 
 * @author Anastaszor
 * @see https://secure.php.net/manual/en/context.php
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class NativeOptions implements Stringable
{
	
	/**
	 * Socket context option listing.
	 * 
	 * @var ?NativeSocketOptions
	 */
	protected ?NativeSocketOptions $_socketOptions = null;
	
	/**
	 * HTTP context option listing.
	 * 
	 * @var ?NativeHttpOptions
	 */
	protected ?NativeHttpOptions $_httpOptions = null;
	
	/**
	 * FTP context option listing.
	 * 
	 * @var ?NativeFtpOptions
	 */
	protected ?NativeFtpOptions $_ftpOptions = null;
	
	/**
	 * SSL context option listing.
	 * 
	 * @var ?NativeSslOptions
	 */
	protected ?NativeSslOptions $_sslOptions = null;
	
	/**
	 * CURL context option listing.
	 * 
	 * @var ?NativeCurlOptions
	 */
	protected ?NativeCurlOptions $_curlOptions = null;
	
	/**
	 * Phar context option listing.
	 * 
	 * @var ?NativePharOptions
	 */
	protected ?NativePharOptions $_pharOptions = null;
	
	/**
	 * MongoDB context option listing.
	 * 
	 * @var ?NativeMongodbOptions
	 */
	protected ?NativeMongodbOptions $_mongodbOptions = null;
	
	/**
	 * Context parameter listing.
	 * 
	 * @var ?NativeContextParameters
	 */
	protected ?NativeContextParameters $_contextParameters = null;
	
	/**
	 * Zip context option listing.
	 * 
	 * @var ?NativeZipOptions
	 */
	protected ?NativeZipOptions $_zipOptions = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the array that will be used to build the context.
	 * 
	 * @return array<string, array<string, null|boolean|integer|float|string|callable>>
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @phpstan-ignore-next-line
	 */
	public function toArray() : array
	{
		$opts = [];
		
		if(null !== $this->_socketOptions)
		{
			$subs = $this->_socketOptions->toArray();
			if([] !== $subs)
			{
				$opts['socket'] = $subs;
			}
		}
		
		if(null !== $this->_httpOptions)
		{
			$subs = $this->_httpOptions->toArray();
			if([] !== $subs)
			{
				$opts['http'] = $subs;
			}
		}
		
		if(null !== $this->_ftpOptions)
		{
			$subs = $this->_ftpOptions->toArray();
			if([] !== $subs)
			{
				$opts['ftp'] = $subs;
			}
		}
		
		if(null !== $this->_sslOptions)
		{
			$subs = $this->_sslOptions->toArray();
			if([] !== $subs)
			{
				$opts['ssl'] = $subs;
			}
		}
		
		if(null !== $this->_curlOptions)
		{
			$subs = $this->_curlOptions->toArray();
			if([] !== $subs)
			{
				$opts['curl'] = $subs;
			}
		}
		
		if(null !== $this->_pharOptions)
		{
			$subs = $this->_pharOptions->toArray();
			if([] !== $subs)
			{
				$opts['phar'] = $subs;
			}
		}
		
		if(null !== $this->_mongodbOptions)
		{
			$subs = $this->_mongodbOptions->toArray();
			if([] !== $subs)
			{
				$opts['mongodb'] = $subs;
			}
		}
		
		if(null !== $this->_zipOptions)
		{
			$subs = $this->_zipOptions->toArray();
			if([] !== $subs)
			{
				$opts['zip'] = $subs;
			}
		}
		
		return $opts;
	}
	
	/**
	 * Gets whether the socket options exist.
	 * 
	 * @return boolean
	 */
	public function hasSocketOptions() : bool
	{
		return null !== $this->_socketOptions;
	}
	
	/**
	 * Gets the socket options.
	 * 
	 * @return NativeSocketOptions
	 */
	public function getSocketOptions() : NativeSocketOptions
	{
		if(null === $this->_socketOptions)
		{
			$this->_socketOptions = new NativeSocketOptions();
		}
		
		return $this->_socketOptions;
	}
	
	/**
	 * Gets whether the http options exist.
	 * 
	 * @return boolean
	 */
	public function hasHttpOptions() : bool
	{
		return null !== $this->_httpOptions;
	}
	
	/**
	 * Gets the http options.
	 * 
	 * @return NativeHttpOptions
	 */
	public function getHttpOptions() : NativeHttpOptions
	{
		if(null === $this->_httpOptions)
		{
			$this->_httpOptions = new NativeHttpOptions();
		}
		
		return $this->_httpOptions;
	}
	
	/**
	 * Gets whether the http options exist.
	 * 
	 * @return boolean
	 */
	public function hasFtpOptions() : bool
	{
		return null !== $this->_ftpOptions;
	}
	
	/**
	 * Gets the ftp options.
	 * 
	 * @return NativeFtpOptions
	 */
	public function getFtpOptions() : NativeFtpOptions
	{
		if(null === $this->_ftpOptions)
		{
			$this->_ftpOptions = new NativeFtpOptions();
		}
		
		return $this->_ftpOptions;
	}
	
	/**
	 * Gets whether the ssl options exist.
	 * 
	 * @return boolean
	 */
	public function hasSslOptions() : bool
	{
		return null !== $this->_sslOptions;
	}
	
	/**
	 * Gets the ssl options.
	 * 
	 * @return NativeSslOptions
	 */
	public function getSslOptions() : NativeSslOptions
	{
		if(null === $this->_sslOptions)
		{
			$this->_sslOptions = new NativeSslOptions();
		}
		
		return $this->_sslOptions;
	}
	
	/**
	 * Gets whether the curl options exist.
	 * 
	 * @return boolean
	 */
	public function hasCurlOptions() : bool
	{
		return null !== $this->_curlOptions;
	}
	
	/**
	 * Gets the curl options.
	 * 
	 * @return NativeCurlOptions
	 */
	public function getCurlOptions() : NativeCurlOptions
	{
		if(null === $this->_curlOptions)
		{
			$this->_curlOptions = new NativeCurlOptions();
		}
		
		return $this->_curlOptions;
	}
	
	/**
	 * Gets whether the phar options exist.
	 * 
	 * @return boolean
	 */
	public function hasPharOptions() : bool
	{
		return null !== $this->_pharOptions;
	}
	
	/**
	 * Gets the phar options.
	 * 
	 * @return NativePharOptions
	 */
	public function getPharOptions() : NativePharOptions
	{
		if(null === $this->_pharOptions)
		{
			$this->_pharOptions = new NativePharOptions();
		}
		
		return $this->_pharOptions;
	}
	
	/**
	 * Gets whether the mongodb options exist.
	 * 
	 * @return boolean
	 */
	public function hasMongodbOptions() : bool
	{
		return null !== $this->_mongodbOptions;
	}
	
	/**
	 * Gets the mongodb options.
	 * 
	 * @return NativeMongodbOptions
	 */
	public function getMongodbOptions() : NativeMongodbOptions
	{
		if(null === $this->_mongodbOptions)
		{
			$this->_mongodbOptions = new NativeMongodbOptions();
		}
		
		return $this->_mongodbOptions;
	}
	
	/**
	 * Gets whether the context parameters exist.
	 * 
	 * @return boolean
	 */
	public function hasContextParameters() : bool
	{
		return null !== $this->_contextParameters;
	}
	
	/**
	 * Gets the context parameters.
	 * 
	 * @return NativeContextParameters
	 */
	public function getContextParameters() : NativeContextParameters
	{
		if(null === $this->_contextParameters)
		{
			$this->_contextParameters = new NativeContextParameters();
		}
		
		return $this->_contextParameters;
	}
	
	/**
	 * Gets whether the zip options exist.
	 * 
	 * @return boolean
	 */
	public function hasZipOptions() : bool
	{
		return null !== $this->_zipOptions;
	}
	
	/**
	 * Gets the zip options.
	 * 
	 * @return NativeZipOptions
	 */
	public function getZipOptions() : NativeZipOptions
	{
		if(null === $this->_zipOptions)
		{
			$this->_zipOptions = new NativeZipOptions();
		}
		
		return $this->_zipOptions;
	}
	
	/**
	 * Applies the context parameters to the given stream http context.
	 * 
	 * @param resource $context
	 */
	public function applyParamsToContext($context) : void
	{
		if(null === $this->_contextParameters)
		{
			return;
		}
		
		$this->_contextParameters->applyParamsToContext($context);
	}
	
	/**
	 * Merges this stream options with the given other options. This method
	 * does not modifies the current options object and creates a new object
	 * with the merged properties of both objects.
	 * 
	 * When merging, if two properties are defined within this object and the
	 * other, the properties of the other object will override the properties
	 * within current object.
	 * 
	 * @param ?NativeOptions $other
	 * @return NativeOptions
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 * @SuppressWarnings("PHPMD.ElseExpression")
	 */
	public function mergeWith(?NativeOptions $other) : NativeOptions
	{
		if(null === $other)
		{
			return $this;
		}
		
		$newobj = new self();
		
		if($this->hasSocketOptions())
		{
			if($other->hasSocketOptions())
			{
				$newobj->_socketOptions = $this->getSocketOptions()->mergeWith($other->getSocketOptions());
			}
			else
			{
				$newobj->_socketOptions = $this->getSocketOptions();
			}
		}
		elseif($other->hasSocketOptions())
		{
			$newobj->_socketOptions = $other->getSocketOptions();
		}
		
		if($this->hasHttpOptions())
		{
			if($other->hasHttpOptions())
			{
				$newobj->_httpOptions = $this->getHttpOptions()->mergeWith($other->getHttpOptions());
			}
			else
			{
				$newobj->_httpOptions = $this->getHttpOptions();
			}
		}
		elseif($other->hasHttpOptions())
		{
			$newobj->_httpOptions = $other->getHttpOptions();
		}
		
		if($this->hasFtpOptions())
		{
			if($other->hasFtpOptions())
			{
				$newobj->_ftpOptions = $this->getFtpOptions()->mergeWith($other->getFtpOptions());
			}
			else
			{
				$newobj->_ftpOptions = $this->getFtpOptions();
			}
		}
		elseif($other->hasFtpOptions())
		{
			$newobj->_ftpOptions = $other->getFtpOptions();
		}
		
		if($this->hasSslOptions())
		{
			if($other->hasSslOptions())
			{
				$newobj->_sslOptions = $this->getSslOptions()->mergeWith($other->getSslOptions());
			}
			else
			{
				$newobj->_sslOptions = $this->getSslOptions();
			}
		}
		elseif($other->hasSslOptions())
		{
			$newobj->_sslOptions = $other->getSslOptions();
		}
		
		if($this->hasCurlOptions())
		{
			if($other->hasCurlOptions())
			{
				$newobj->_curlOptions = $this->getCurlOptions()->mergeWith($other->getCurlOptions());
			}
			else
			{
				$newobj->_curlOptions = $this->getCurlOptions();
			}
		}
		elseif($other->hasCurlOptions())
		{
			$newobj->_curlOptions = $other->getCurlOptions();
		}
		
		if($this->hasPharOptions())
		{
			if($other->hasPharOptions())
			{
				$newobj->_pharOptions = $this->getPharOptions()->mergeWith($other->getPharOptions());
			}
			else
			{
				$newobj->_pharOptions = $this->getPharOptions();
			}
		}
		elseif($other->hasPharOptions())
		{
			$newobj->_pharOptions = $other->getPharOptions();
		}
		
		if($this->hasMongodbOptions())
		{
			if($other->hasMongodbOptions())
			{
				$newobj->_mongodbOptions = $this->getMongodbOptions()->mergeWith($other->getMongodbOptions());
			}
			else
			{
				$newobj->_mongodbOptions = $this->getMongodbOptions();
			}
		}
		elseif($other->hasMongodbOptions())
		{
			$newobj->_mongodbOptions = $other->getMongodbOptions();
		}
		
		if($this->hasContextParameters())
		{
			if($other->hasContextParameters())
			{
				$newobj->_contextParameters = $this->getContextParameters()->mergeWith($other->getContextParameters());
			}
			else
			{
				$newobj->_contextParameters = $this->getContextParameters();
			}
		}
		elseif($other->hasContextParameters())
		{
			$newobj->_contextParameters = $other->getContextParameters();
		}
		
		if($this->hasZipOptions())
		{
			if($other->hasZipOptions())
			{
				$newobj->_zipOptions = $this->getZipOptions()->mergeWith($other->getZipOptions());
			}
			else
			{
				$newobj->_zipOptions = $this->getZipOptions();
			}
		}
		elseif($other->hasZipOptions())
		{
			$newobj->_zipOptions = $other->getZipOptions();
		}
		
		return $newobj;
	}
	
}

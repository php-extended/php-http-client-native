<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-native library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Stringable;

/**
 * NativeZipOptions class file.
 * 
 * This class represents all the options that are used for the zip stream
 * wrapper.
 * 
 * @author Anastaszor
 * @see https://secure.php.net/manual/en/context.zip.php
 */
class NativeZipOptions implements Stringable
{
	
	/**
	 * Used to specify password used for encrypted archive.
	 * 
	 * @var ?string
	 */
	protected ?string $_password = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the option values as array.
	 * 
	 * @return array<string, string>
	 */
	public function toArray() : array
	{
		$opts = [];
		
		if(null !== $this->_password)
		{
			$opts['password'] = $this->_password;
		}
		
		return $opts;
	}
	
	/**
	 * Gets the password of the zip archive.
	 * 
	 * @return ?string
	 */
	public function getPassword() : ?string
	{
		return $this->_password;
	}
	
	/**
	 * Sets the password of the zip archive.
	 * 
	 * @param string $password
	 */
	public function setPassword(string $password) : void
	{
		$this->_password = $password;
	}
	
	/**
	 * Merges this stream options with the given other options. This method
	 * does not modifies the current options object and creates a new object
	 * with the merged properties of both objects.
	 *
	 * When merging, if two properties are defined within this object and the
	 * other, the properties of the other object will override the properties
	 * within current object.
	 *
	 * @param ?NativeZipOptions $other
	 * @return NativeZipOptions
	 */
	public function mergeWith(?NativeZipOptions $other) : NativeZipOptions
	{
		if(null === $other)
		{
			return $this;
		}
		
		$newobj = new self();
		$newobj->_password = $other->getPassword() ?? $this->getPassword();
		
		return $newobj;
	}
	
}

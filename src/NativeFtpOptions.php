<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-native library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Stringable;

/**
 * NativeFtpOptions class file.
 * 
 * This class represents all the options that are used for the ftp stream
 * wrapper.
 * 
 * @author Anastaszor
 * @see https://secure.php.net/manual/en/context.ftp.php
 */
class NativeFtpOptions implements Stringable
{
	
	/**
	 * Allows overwriting of files (writing only).
	 * 
	 * @var boolean
	 */
	protected bool $_overwrite = false;
	
	/**
	 * File offset at which to begin transfer (reading only).
	 * 
	 * @var integer
	 */
	protected int $_resumePos = 0;
	
	/**
	 * The ftp proxy address.
	 * 
	 * @var ?string
	 */
	protected ?string $_proxy = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the array that will be used to build the context.
	 *
	 * @return array<string, boolean|integer|string>
	 */
	public function toArray() : array
	{
		$opts = [
			'overwrite' => $this->_overwrite,
			'resume_pos' => $this->_resumePos,
		];
		
		if(null !== $this->_proxy && '' !== $this->_proxy)
		{
			$opts['proxy'] = $this->_proxy;
		}
		
		return $opts;
	}
	
	/**
	 * Gets whether to overwrite the files on the server.
	 * 
	 * @return boolean
	 */
	public function shouldOverwrite() : bool
	{
		return $this->_overwrite;
	}
	
	/**
	 * Gets the offset at which the position begins.
	 * 
	 * @return integer
	 */
	public function getResumePos() : int
	{
		return $this->_resumePos;
	}
	
	/**
	 * Gets the proxy address.
	 * 
	 * @return ?string
	 */
	public function getProxy() : ?string
	{
		return $this->_proxy;
	}
	
	/**
	 * Sets whether to overwrite files.
	 * 
	 * @param boolean $overwrite
	 */
	public function setOverwrite(bool $overwrite) : void
	{
		$this->_overwrite = $overwrite;
	}
	
	/**
	 * Sets the offset at which to read files.
	 * 
	 * @param integer $resumePos
	 */
	public function setResumePos(int $resumePos) : void
	{
		$this->_resumePos = $resumePos;
	}
	
	/**
	 * Sets the ftp proxy address.
	 * 
	 * @param string $proxy
	 */
	public function setProxy(string $proxy) : void
	{
		$this->_proxy = $proxy;
	}
	
	/**
	 * Merges this stream options with the given other options. This method
	 * does not modifies the current options object and creates a new object
	 * with the merged properties of both objects.
	 *
	 * When merging, if two properties are defined within this object and the
	 * other, the properties of the other object will override the properties
	 * within current object.
	 *
	 * @param ?NativeFtpOptions $other
	 * @return NativeFtpOptions
	 */
	public function mergeWith(?NativeFtpOptions $other) : NativeFtpOptions
	{
		if(null === $other)
		{
			return $this;
		}
		
		$newobj = new self();
		$newobj->_overwrite = $other->shouldOverwrite() || $this->shouldOverwrite();
		$newobj->_resumePos = \max($other->getResumePos(), $this->getResumePos());
		$newobj->_proxy = $other->getProxy() ?? $this->getProxy();
		
		return $newobj;
	}
	
}

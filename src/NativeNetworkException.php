<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-native library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Psr\Http\Client\NetworkExceptionInterface;
use Psr\Http\Message\RequestInterface;
use RuntimeException;
use Throwable;

/**
 * NativeNetworkException class file.
 * 
 * This class represents a network error when fetching the contents.
 * 
 * @author Anastaszor
 */
class NativeNetworkException extends RuntimeException implements NetworkExceptionInterface
{
	
	/**
	 * The request that triggered the exception.
	 * 
	 * @var RequestInterface
	 */
	protected RequestInterface $_request;
	
	/**
	 * Builds a new NativeNetworkException with the given request and previous
	 * exceptions.
	 * 
	 * @param RequestInterface $request
	 * @param string $message
	 * @param integer $code
	 * @param Throwable $previous
	 */
	public function __construct(RequestInterface $request, string $message, int $code = -1, ?Throwable $previous = null)
	{
		$this->_request = $request;
		parent::__construct($message, $code, $previous);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Client\NetworkExceptionInterface::getRequest()
	 */
	public function getRequest() : RequestInterface
	{
		return $this->_request;
	}
	
}

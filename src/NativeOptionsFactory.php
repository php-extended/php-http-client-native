<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-native library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use InvalidArgumentException;
use Psr\Http\Message\RequestInterface;
use Stringable;

/**
 * NativeOptionsFactory class file.
 * 
 * This class is to build stream context options objects with the given
 * requests.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class NativeOptionsFactory implements Stringable
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Builds the options for the context from the request.
	 * 
	 * @param RequestInterface $request
	 * @return NativeOptions
	 */
	public function createFromRequest(RequestInterface $request) : NativeOptions
	{
		$context = new NativeOptions();
		
		$context->getHttpOptions()->setProtocolVersion($request->getProtocolVersion());
		$context->getHttpOptions()->setContent($request->getBody()->__toString());
		$context->getHttpOptions()->setMethod($request->getMethod());
		
		// custom header messages or configuration from earlier decorative clients
		$followLocationHeader = $request->getHeaderLine('X-Php-Follow-Location');
		if(0 < \mb_strlen($followLocationHeader))
		{
			$context->getHttpOptions()->setFollowLocation((bool) $followLocationHeader);
		}
		
		// {{{ Remove X-Php-* headers for privacy
		$headers = $request->getHeaders();
		$keys = \array_keys($headers);
		
		foreach($keys as $headerName)
		{
			if(0 === \mb_stripos((string) $headerName, 'X-Php'))
			{
				unset($headers[$headerName]);
			}
		}
		// }}}
		
		// all remaining headers are transmitted in the options
		/** @psalm-suppress MixedArgumentTypeCoercion */
		$context->getHttpOptions()->setHeaders($headers);
		
		return $context;
	}
	
	/**
	 * Builds the options for the context from the request. This method ignores
	 * all keys that are not supported.
	 * 
	 * @param array<string, array<string, null|boolean|integer|float|string|object|callable|array<integer|string, null|boolean|integer|float|string|object>>> $request
	 * @return NativeOptions
	 * @throws InvalidArgumentException if not silent and failed to set a
	 *                                  parameter
	 * @phpstan-ignore-next-line
	 */
	public function createFromContextArray(array $request) : NativeOptions
	{
		$keys = ['socket', 'http', 'https', 'ftp', 'ftps', 'ssl', 'tls', 'curl', 'phar', 'mongo', 'mongodb', 'context', 'params', 'parameters', 'zip'];
		$context = new NativeOptions();
		
		foreach($request as $key => $contents)
		{
			switch($key)
			{
				case 'socket':
					/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidArgument */
					$this->createFromSocketOptions($context->getSocketOptions(), $contents);
					break;
				
				case 'http':
				case 'https':
					/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidArgument */
					$this->createFromHttpOptions($context->getHttpOptions(), $contents);
					break;
				
				case 'ftp':
				case 'ftps':
					/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidArgument */
					$this->createFromFtpOptions($context->getFtpOptions(), $contents);
					break;
				
				case 'ssl':
				case 'tls':
					/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidArgument */
					$this->createFromSslOptions($context->getSslOptions(), $contents);
					break;
				
				case 'curl':
					/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidArgument */
					$this->createFromCurlOptions($context->getCurlOptions(), $contents);
					break;
				
				case 'phar':
					/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidArgument */
					$this->createFromPharOptions($context->getPharOptions(), $contents);
					break;
				
				case 'mongo':
				case 'mongodb':
					$this->createFromMongodbOptions($context->getMongodbOptions(), $contents);
					break;
				
				case 'context':
				case 'params':
				case 'parameters':
					$this->createFromContextParameters($context->getContextParameters(), $contents);
					break;
				
				case 'zip':
					/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidArgument */
					$this->createFromZipOptions($context->getZipOptions(), $contents);
					break;
				
				default:
					throw $this->onInvalidKey($key, $context, $keys);
			}
		}
		
		return $context;
	}
	
	/**
	 * Throws an invalid argument exception for invalid key.
	 * 
	 * @param string $key
	 * @param Stringable $options
	 * @param array<integer, string> $keys
	 * @return InvalidArgumentException
	 */
	public function onInvalidKey(string $key, Stringable $options, array $keys) : InvalidArgumentException
	{
		$message = 'Failed to set option {key} in object {class}, the supported keys are {keys}.';
		$context = [
			'{key}' => $key,
			'{class}' => \get_class($options),
			'{keys}' => \implode(', ', $keys),
		];
		
		return new InvalidArgumentException(\strtr($message, $context));
	}
	
	/**
	 * Builds the options for the context from the request. This method ignores
	 * all keys that are not supported.
	 * 
	 * @param NativeSocketOptions $options
	 * @param array<string, null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string>> $contents
	 * @throws InvalidArgumentException if not silent and failed to set a parameter
	 */
	protected function createFromSocketOptions(NativeSocketOptions $options, array $contents) : void
	{
		$keys = ['bind_to', 'backlog', 'ipv6_v6only', 'so_reuseport', 'so_broadcast'];
		
		foreach($contents as $key => $value)
		{
			switch($key)
			{
				case 'bind_to':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setBindTo((string) $value);
					break;
				
				case 'backlog':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setBacklog((int) $value);
					break;
				
				case 'ipv6_v6only':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setIpv6V6only((bool) $value);
					break;
				
				case 'so_reuseport':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setSoReuseport((bool) $value);
					break;
				
				case 'so_broadcast':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setSoBroadcast((bool) $value);
					break;
				
				default:
					throw $this->onInvalidKey($key, $options, $keys);
			}
		}
	}
	
	/**
	 * Builds the options for the context from the request. This method ignores
	 * all keys that are not supported.
	 * 
	 * @param NativeHttpOptions $options
	 * @param array<string, null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string>> $contents
	 * @throws InvalidArgumentException if not silent and failed to set a
	 *                                  parameter
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	protected function createFromHttpOptions(NativeHttpOptions $options, array $contents) : void
	{
		$keys = ['method', 'header', 'user_agent', 'contents', 'proxy', 'request_fulluri', 'follow_location', 'max_redirects', 'protocol_version', 'timeout', 'ignore_errors'];
		
		foreach($contents as $key => $value)
		{
			switch($key)
			{
				case 'method':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setMethod((string) $value);
					break;
				
				case 'headers':
				case 'header':
					if(\is_scalar($value))
					{
						$value = \explode("\r\n", (string) $value);
					}
					if(!\is_array($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidArgument */
					$options->setHeaders((array) $value);
					break;
				
				case 'user_agent':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setUserAgent((string) $value);
					break;
				
				case 'content':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setContent((string) $value);
					break;
				
				case 'proxy':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setProxy((string) $value);
					break;
				
				case 'request_uri':
				case 'request_fulluri':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setRequestFulluri((bool) $value);
					break;
				
				case 'follow_location':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setFollowLocation((bool) $value);
					break;
				
				case 'max_redirects':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setMaxRedirects((int) $value);
					break;
				
				case 'protocol_version':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setProtocolVersion((string) $value);
					break;
				
				case 'timeout':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setTimeout((int) $value);
					break;
				
				case 'ignore_errors':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setIgnoreErrors((bool) $value);
					break;
				
				default:
					throw $this->onInvalidKey($key, $options, $keys);
			}
		}
	}
	
	/**
	 * Builds the options for the context from the request. This method ignores
	 * all keys that are not supported.
	 * 
	 * @param NativeFtpOptions $options
	 * @param array<string, null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string>> $contents
	 * @throws InvalidArgumentException if not silent and failed to set a
	 *                                  parameter
	 */
	protected function createFromFtpOptions(NativeFtpOptions $options, array $contents) : void
	{
		$keys = ['overwrite', 'resume_pos', 'proxy'];
		
		foreach($contents as $key => $value)
		{
			switch($key)
			{
				case 'overwrite':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setOverwrite((bool) $value);
					break;
				
				case 'resume_pos':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setResumePos((int) $value);
					break;
				
				case 'proxy':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setProxy((string) $value);
					break;
				
				default:
					throw $this->onInvalidKey($key, $options, $keys);
			}
		}
	}
	
	/**
	 * Builds the options for the context from the request. This method ignores
	 * all keys that are not supported.
	 * 
	 * @param NativeSslOptions $options
	 * @param array<string, null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string>> $contents
	 * @throws InvalidArgumentException if not silent and failed to set a parameter
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	protected function createFromSslOptions(NativeSslOptions $options, array $contents) : void
	{
		$keys = ['peer_name', 'verify_peer', 'verify_peer_name', 'allow_self_signed', 'cafile', 'capath', 'local_cert', 'local_pk', 'passphrase', 'cn_match', 'verify_depths', 'ciphers', 'capture_peer_cert', 'capture_peer_cert_chain', 'sni_enabled', 'sni_server_name', 'disable_compression', 'peer_fingerprint'];
		
		foreach($contents as $key => $value)
		{
			switch($key)
			{
				case 'cn_match':
				case 'sni_server_name':
				case 'peer_name':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setPeerName((string) $value);
					break;
				
				case 'verify_peer':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setVerifyPeer((bool) $value);
					break;
				
				case 'verify_peer_name':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setVerifyPeerName((bool) $value);
					break;
				
				case 'allow_self_signed':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setAllowSelfSigned((bool) $value);
					break;
				
				case 'cafile':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setCafile((string) $value);
					break;
				
				case 'capath':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setCapath((string) $value);
					break;
				
				case 'local_cert':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setLocalCert((string) $value);
					break;
				
				case 'local_pk':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setLocalPk((string) $value);
					break;
				
				case 'passphrase':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setPassphrase((string) $value);
					break;
				
				case 'verify_depths':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setVerifyDepths((int) $value);
					break;
				
				case 'ciphers':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setCiphers((string) $value);
					break;
				
				case 'capture_peer_cert':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setCapturePeerCert((bool) $value);
					break;
				
				case 'capture_peer_cert_chain':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setCapturePeerCertChain((bool) $value);
					break;
				
				case 'sni_enabled':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setSniEnabled((bool) $value);
					break;
				
				case 'disable_compression':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setDisableCompression((bool) $value);
					break;
				
				case 'peer_fingerprint':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setPeerFingerprint((string) $value);
					break;
				
				default:
					throw $this->onInvalidKey($key, $options, $keys);
			}
		}
	}
	
	/**
	 * Builds the options for the context from the request. This method ignores
	 * all keys that are not supported.
	 * 
	 * @param NativeCurlOptions $options
	 * @param array<string, null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string>> $contents
	 * @throws InvalidArgumentException if not silent and failed to set a
	 *                                  parameter
	 */
	protected function createFromCurlOptions(NativeCurlOptions $options, array $contents) : void
	{
		$keys = ['method', 'header', 'user_agent', 'content', 'proxy', 'max_redirects', 'curl_verify_ssl_host', 'curl_verify_ssl_peer'];
		
		foreach($contents as $key => $value)
		{
			switch($key)
			{
				case 'method':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setMethod((string) $value);
					break;
				
				case 'headers':
				case 'header':
					if(\is_scalar($value))
					{
						$value = \explode("\r\n", (string) $value);
					}
					if(!\is_array($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidArgument */
					$options->setHeaders((array) $value);
					break;
				
				case 'user_agent':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setUserAgent((string) $value);
					break;
				
				case 'content':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setContent((string) $value);
					break;
				
				case 'proxy':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setProxy((string) $value);
					break;
				
				case 'max_redirects':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setMaxRedirects((int) $value);
					break;
				
				case 'curl_verify_ssl_host':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setCurlVerifySslHost((bool) $value);
					break;
				
				case 'curl_verify_ssl_peer':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setCurlVerifySslPeer((bool) $value);
					break;
				
				default:
					throw $this->onInvalidKey($key, $options, $keys);
			}
		}
	}
	
	/**
	 * Builds the options for the context from the request. This method ignores
	 * all keys that are not supported.
	 * 
	 * @param NativePharOptions $options
	 * @param array<string, null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string>> $contents
	 * @throws InvalidArgumentException if not silent and failed to set a
	 *                                  parameter
	 */
	protected function createFromPharOptions(NativePharOptions $options, array $contents) : void
	{
		$keys = ['compress', 'metadata'];
		
		foreach($contents as $key => $value)
		{
			switch($key)
			{
				case 'compress':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setCompress((int) $value);
					break;
				
				case 'metadata':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setMetadata($value);
					break;
				
				default:
					throw $this->onInvalidKey($key, $options, $keys);
			}
		}
	}
	
	/**
	 * Builds the options for the context from the request. This method ignores
	 * all keys that are not supported.
	 *
	 * @param NativeMongodbOptions $options
	 * @param array<string, null|boolean|integer|float|string|object|callable|array<integer|string, null|boolean|integer|float|string|object>> $contents
	 * @throws InvalidArgumentException if not silent and failed to set a
	 *                                  parameter
	 * @phpstan-ignore-next-line
	 */
	protected function createFromMongodbOptions(NativeMongodbOptions $options, array $contents) : void
	{
		$keys = ['log_cmd_insert', 'log_cmd_delete', 'log_cmd_update', 'log_write_batch', 'log_reply', 'log_getmore', 'log_killcursor'];
		
		foreach($contents as $key => $value)
		{
			switch($key)
			{
				case 'log_cmd_insert':
					if(!\is_callable($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setLogCmdInsert($value);
					break;
				
				case 'log_cmd_delete':
					if(!\is_callable($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setLogCmdDelete($value);
					break;
				
				case 'log_cmd_update':
					if(!\is_callable($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setLogCmdUpdate($value);
					break;
				
				case 'log_write_batch':
					if(!\is_callable($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setLogWriteBatch($value);
					break;
				
				case 'log_reply':
					if(!\is_callable($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setLogReply($value);
					break;
				
				case 'log_getmore':
					if(!\is_callable($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setLogGetmore($value);
					break;
				
				case 'log_killcursor':
					if(!\is_callable($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setLogKillcursor($value);
					break;
				
				default:
					throw $this->onInvalidKey($key, $options, $keys);
			}
		}
	}
	
	/**
	 * Builds the options for the context from the request. This method ignores
	 * all keys that are not supported.
	 *
	 * @param NativeContextParameters $params
	 * @param array<string, null|boolean|integer|float|string|object|callable|array<integer|string, null|boolean|integer|float|string|object>> $contents
	 * @throws InvalidArgumentException if not silent and failed to set a parameter
	 * @phpstan-ignore-next-line
	 */
	protected function createFromContextParameters(NativeContextParameters $params, array $contents) : void
	{
		$keys = ['notification'];
		
		foreach($contents as $key => $value)
		{
			switch($key)
			{
				case 'notification':
					if(!\is_callable($value))
					{
						throw $this->onInvalidKey($key, $params, $keys);
					}
					
					$params->setNotification($value);
					break;
				
				default:
					throw $this->onInvalidKey($key, $params, $keys);
			}
		}
	}
	
	/**
	 * Builds the options for the context from the request. This method ignores
	 * all keys that are not supported.
	 *
	 * @param NativeZipOptions $options
	 * @param array<string, null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string>> $contents
	 * @throws InvalidArgumentException if not silent and failed to set a parameter
	 */
	protected function createFromZipOptions(NativeZipOptions $options, array $contents) : void
	{
		$keys = ['password'];
		
		foreach($contents as $key => $value)
		{
			switch($key)
			{
				case 'password':
					if(!\is_scalar($value))
					{
						throw $this->onInvalidKey($key, $options, $keys);
					}
					
					$options->setPassword((string) $value);
					break;
				
				default:
					throw $this->onInvalidKey($key, $options, $keys);
			}
		}
	}
	
}

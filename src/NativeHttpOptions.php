<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-native library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Stringable;

/**
 * NativeContextOptions class file.
 * 
 * This class represents all the options that are used for the http stream
 * wrapper.
 * 
 * @author Anastaszor
 * @see https://secure.php.net/manual/en/context.http.php
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class NativeHttpOptions implements Stringable
{
	
	/**
	 * The method of the http request.
	 * 
	 * @var string
	 */
	protected string $_method = 'GET';
	
	/**
	 * The additional headers to put to all requests.
	 * 
	 * @var array<string, array<int, string>>
	 */
	protected array $_headers = [];
	
	/**
	 * The user agent that will be used to make the request.
	 * 
	 * @var ?string
	 */
	protected ?string $_userAgent = null;
	
	/**
	 * The internal contents of the request.
	 * 
	 * @var ?string
	 */
	protected ?string $_content = null;
	
	/**
	 * The address:port of the proxy to use.
	 * 
	 * @var ?string
	 */
	protected ?string $_proxy = null;
	
	/**
	 * Whether to send the full request uri.
	 * 
	 * @var boolean
	 */
	protected bool $_requestFulluri = true;
	
	/**
	 * Whether to follow locations given by the response.
	 * 
	 * @var boolean
	 */
	protected bool $_followLocation = true;
	
	/**
	 * How many redirections max should we follow (to avoid infinite redirects).
	 * 
	 * @var integer
	 */
	protected int $_maxRedirects = 20;
	
	/**
	 * The http protocol version to be used.
	 * 
	 * @var string
	 */
	protected string $_protocolVersion = '1.1';
	
	/**
	 * The maximum time allowed to the request.
	 * 
	 * @var integer
	 */
	protected int $_timeout = 60;
	
	/**
	 * Whether to ignore the status code given by the server.
	 * 
	 * @var boolean
	 */
	protected bool $_ignoreErrors = true;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the array that will be used to build the context.
	 *
	 * @return array<string, boolean|integer|string>
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function toArray() : array
	{
		$strheaders = [];
		$cookieh = [];
		
		// handle all other headers
		foreach($this->_headers as $key => $headers)
		{
			if('Cookie' === $key)
			{
				foreach($headers as $header)
				{
					$cookieh[] = $header;
				}
				
				continue;
			}
			
			foreach($headers as $header)
			{
				$strheaders[] = $key.': '.$header;
			}
		}
		
		if(\count($cookieh) > 0)
		{
			// handle cookie concatenation and unicity
			$strheaders[] = 'Cookie: '.\implode('; ', $cookieh);
		}
		
		$opts = [
			'follow_location' => (int) $this->_followLocation,
			'ignore_errors' => $this->_ignoreErrors,
			'max_redirects' => $this->_maxRedirects,
			'method' => $this->_method,
			'protocol_version' => $this->_protocolVersion,
			'request_fulluri' => $this->_requestFulluri,
			'timeout' => $this->_timeout,
		];
		
		if(null !== $this->_content && '' !== $this->_content)
		{
			$opts['content'] = $this->_content;
		}
		
		if(\count($strheaders) > 0)
		{
			$opts['header'] = \implode("\r\n", $strheaders)."\r\n";
		}
		
		if(null !== $this->_userAgent && '' !== $this->_userAgent)
		{
			$opts['user_agent'] = $this->_userAgent;
		}
		
		if(null !== $this->_proxy && '' !== $this->_proxy)
		{
			$opts['proxy'] = $this->_proxy;
		}
		
		return $opts;
	}
	
	/**
	 * Gets the http verb that will be used.
	 * 
	 * @return string
	 */
	public function getMethod() : string
	{
		return $this->_method;
	}
	
	/**
	 * Gets the headers that will be sent.
	 * 
	 * @return array<string, array<int, string>>
	 */
	public function getHeaders() : array
	{
		return $this->_headers;
	}
	
	/**
	 * Gets the user agent string that will be used.
	 * 
	 * @return ?string
	 */
	public function getUserAgent() : ?string
	{
		return $this->_userAgent;
	}
	
	/**
	 * Gets the raw contents that will be used.
	 * 
	 * @return ?string
	 */
	public function getContent() : ?string
	{
		return $this->_content;
	}
	
	/**
	 * Gets the proxy address the flux will pass through.
	 * 
	 * @return ?string
	 */
	public function getProxy() : ?string
	{
		return $this->_proxy;
	}
	
	/**
	 * Gets whether the full request uri should be used.
	 * 
	 * @return boolean
	 */
	public function shouldRequestFulluri() : bool
	{
		return $this->_requestFulluri;
	}
	
	/**
	 * Gets whether this should follow the locations.
	 * 
	 * @return boolean
	 */
	public function shouldFollowLocation() : bool
	{
		return $this->_followLocation;
	}
	
	/**
	 * Gets the maximum number of redirects. Only used if follow_location is
	 * set to true.
	 * 
	 * @return integer
	 */
	public function getMaxRedirects() : int
	{
		return $this->_maxRedirects;
	}
	
	/**
	 * Gets the protocol version that will be used.
	 * 
	 * @return string
	 */
	public function getProtocolVersion() : string
	{
		return $this->_protocolVersion;
	}
	
	/**
	 * Gets the timeout of the request (in seconds).
	 * 
	 * @return integer
	 */
	public function getTimeout() : int
	{
		return $this->_timeout;
	}
	
	/**
	 * Gets whether to ignore non successful server response codes.
	 * 
	 * @return boolean
	 */
	public function shouldIgnoreErrors() : bool
	{
		return $this->_ignoreErrors;
	}
	
	/**
	 * Sets the method that will be used.
	 * 
	 * @param string $method
	 */
	public function setMethod(string $method) : void
	{
		$this->_method = $method;
	}
	
	/**
	 * Adds all the additional headers given in the array. This array values 
	 * may be strings, or an array of strings, which represents different
	 * values for the same header. All values will be appened to the already
	 * present values of the headers.
	 * 
	 * @param array<string, string|array<int, string>> $headers
	 */
	public function setHeaders(array $headers) : void
	{
		$this->_headers = [];
		
		foreach($headers as $headerName => $headerValues)
		{
			if(\is_string($headerValues))
			{
				$this->addHeader($headerName, $headerValues);
			}
			
			if(\is_array($headerValues))
			{
				foreach($headerValues as $headerValue)
				{
					$this->addHeader($headerName, $headerValue);
				}
			}
		}
	}
	
	/**
	 * Adds a specific header to the header list.
	 * 
	 * @param string $headerName
	 * @param string $headerValue
	 */
	public function addHeader(string $headerName, string $headerValue) : void
	{
		$this->_headers[$headerName][] = $headerValue;
	}
	
	/**
	 * Sets the user agent value. This value will be used if it is not already
	 * used into the other headers.
	 * 
	 * @param string $userAgent
	 */
	public function setUserAgent(string $userAgent) : void
	{
		$this->_userAgent = $userAgent;
	}
	
	/**
	 * Sets the inner contents of the request.
	 * 
	 * @param string $content
	 */
	public function setContent(string $content) : void
	{
		$this->_content = $content;
	}
	
	/**
	 * Sets the proxy address the connection should pass through.
	 * 
	 * @param string $proxy
	 */
	public function setProxy(string $proxy) : void
	{
		$this->_proxy = $proxy;
	}
	
	/**
	 * Sets whether the full uri should be sent.
	 * 
	 * @param boolean $requestFulluri
	 */
	public function setRequestFulluri(bool $requestFulluri) : void
	{
		$this->_requestFulluri = $requestFulluri;
	}
	
	/**
	 * Sets whether the engine should follow the locations given by the server.
	 * 
	 * @param boolean $followLocation
	 */
	public function setFollowLocation(bool $followLocation) : void
	{
		$this->_followLocation = $followLocation;
	}
	
	/**
	 * Sets the maximum number of redirects the engine should follow.
	 * 
	 * @param integer $maxRedirects
	 */
	public function setMaxRedirects(int $maxRedirects) : void
	{
		$this->_maxRedirects = $maxRedirects;
	}
	
	/**
	 * Sets the protocol version to use.
	 * 
	 * @param string $protocolVersion
	 */
	public function setProtocolVersion(string $protocolVersion) : void
	{
		$this->_protocolVersion = $protocolVersion;
	}
	
	/**
	 * Sets the maximum execution time for one request (in seconds).
	 * 
	 * @param integer $timeout
	 */
	public function setTimeout(int $timeout) : void
	{
		$this->_timeout = $timeout;
	}
	
	/**
	 * Sets whether to ignore the server response code if it represents an
	 * error code.
	 * 
	 * @param boolean $ignoreErrors
	 */
	public function setIgnoreErrors(bool $ignoreErrors) : void
	{
		$this->_ignoreErrors = $ignoreErrors;
	}
	
	/**
	 * Merges this stream options with the given other options. This method
	 * does not modifies the current options object and creates a new object
	 * with the merged properties of both objects.
	 *
	 * When merging, if two properties are defined within this object and the
	 * other, the properties of the other object will override the properties
	 * within current object.
	 *
	 * @param ?NativeHttpOptions $other
	 * @return NativeHttpOptions
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function mergeWith(?NativeHttpOptions $other) : NativeHttpOptions
	{
		if(null === $other)
		{
			return $this;
		}
		
		$newopt = new self();
		$newopt->_method = $other->getMethod();
		
		foreach($this->getHeaders() as $hkey => $hhs)
		{
			foreach($hhs as $header)
			{
				$newopt->_headers[$hkey][] = $header;
			}
		}
		
		foreach($other->getHeaders() as $hkey => $hhs)
		{
			foreach($hhs as $header)
			{
				$newopt->_headers[$hkey][] = $header;
			}
		}
		
		$newopt->_userAgent = $other->getUserAgent() ?? $this->getUserAgent();
		$newopt->_content = $other->getContent() === null || $other->getContent() === '' ? $this->getContent() : $other->getContent();
		$newopt->_proxy = $other->getProxy() ?? $this->getProxy();
		$newopt->_requestFulluri = $other->shouldRequestFulluri() || $this->shouldRequestFulluri();
		$newopt->_followLocation = $other->shouldFollowLocation() && $this->shouldFollowLocation();
		$newopt->_maxRedirects = \min($other->getMaxRedirects(), $this->getMaxRedirects());
		$newopt->_protocolVersion = $other->getProtocolVersion();
		$newopt->_timeout = \max($other->getTimeout(), $this->getTimeout());
		$newopt->_ignoreErrors = $other->shouldIgnoreErrors() || $this->shouldIgnoreErrors();
		
		return $newopt;
	}
	
}

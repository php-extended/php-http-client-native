<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-native library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Stringable;

/**
 * NativeSocketOptions class file.
 * 
 * This class represents all the options that are used for the socket stream
 * wrapper.
 * 
 * @author Anastaszor
 * @see https://secure.php.net/manual/en/context.socket.php
 */
class NativeSocketOptions implements Stringable
{
	
	/**
	 * The specific ip:port to bind to. Use the syntax ip:port for ipv4, and
	 * [ip]:port for ipv6. A zero in the ip will let the system choose the ip,
	 * and a zero in the port will let the system choose the port.
	 * 
	 * @var ?string
	 */
	protected ?string $_bindTo = null;
	
	/**
	 * The limit of outer connections to the socket listen queue.
	 * 
	 * @var integer
	 */
	protected int $_backlog = 100;
	
	/**
	 * Whether system properties when trying to bind to ipv6 addresses are
	 * overridden.
	 * 
	 * @var boolean
	 */
	protected bool $_ipv6Only = false;
	
	/**
	 * Whether multiple bindings on the same ip:port is allowed.
	 * 
	 * @var boolean
	 */
	protected bool $_soReuseport = true;
	
	/**
	 * Whether bindings on the broadcast addresses is allowed.
	 * 
	 * @var boolean
	 */
	protected bool $_soBroadcast = true;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the option values as array.
	 *
	 * @return array<string, boolean|integer|string>
	 */
	public function toArray() : array
	{
		$opts = [
			'backlog' => $this->_backlog,
			'ipv6_v6only' => $this->_ipv6Only,
			'so_broadcast' => $this->_soBroadcast,
			'so_reuseport' => $this->_soReuseport,
		];
		
		if(null !== $this->_bindTo && '' !== $this->_bindTo)
		{
			$opts['bind_to'] = $this->_bindTo;
		}
		
		return $opts;
	}
	
	/**
	 * Gets the ip:port address to bind to.
	 * 
	 * @return ?string
	 */
	public function getBindTo() : ?string
	{
		return $this->_bindTo;
	}
	
	/**
	 * Gets the number of outer connections in the listen queue.
	 * 
	 * @return integer
	 */
	public function getBacklog() : int
	{
		return $this->_backlog;
	}
	
	/**
	 * Gets whether the ipv6 should be used regardless of system settings.
	 * 
	 * @return boolean
	 */
	public function shouldIpv6Only() : bool
	{
		return $this->_ipv6Only;
	}
	
	/**
	 * Gets whether multiple binding is allowed on the same ip:port.
	 * 
	 * @return boolean
	 */
	public function shouldSoReuseport() : bool
	{
		return $this->_soReuseport;
	}
	
	/**
	 * Gets whether the binding is allowed on the broadcast addresses.
	 * 
	 * @return boolean
	 */
	public function shouldSoBroadcast() : bool
	{
		return $this->_soBroadcast;
	}
	
	/**
	 * Forces the socket to bind to a specific ip. Use the syntax
	 * ip:port for ipv4, and [ip]:port for ipv6. A zero in the ip will let
	 * the system choose the ip, and a zero in the port will let the system
	 * choose the port.
	 * 
	 * @param string $bindTo
	 */
	public function setBindTo(string $bindTo) : void
	{
		$this->_bindTo = $bindTo;
	}
	
	/**
	 * Forces this socket to bind to ipv4.
	 */
	public function forceBindToIpv4() : void
	{
		$this->setBindTo('0:0');
	}
	
	/**
	 * Forces this socket to bind to ipv6.
	 */
	public function forceBindToIpv6() : void
	{
		$this->setBindTo('[0]:0');
	}
	
	/**
	 * Sets the limit of outer connections to the socket listen queue.
	 * 
	 * @param integer $backlog
	 */
	public function setBacklog(int $backlog) : void
	{
		$this->_backlog = $backlog;
	}
	
	/**
	 * Overrides system properties when trying to bind to ipv6 addresses.
	 * 
	 * @param boolean $ipv6Only
	 */
	public function setIpv6V6only(bool $ipv6Only) : void
	{
		$this->_ipv6Only = $ipv6Only;
	}
	
	/**
	 * Allows multiple bindings on the same ip:port.
	 * 
	 * @param boolean $soReuseport
	 */
	public function setSoReuseport(bool $soReuseport) : void
	{
		$this->_soReuseport = $soReuseport;
	}
	
	/**
	 * Allows bindings on the broadcast addresses.
	 * 
	 * @param boolean $soBroadcast
	 */
	public function setSoBroadcast(bool $soBroadcast) : void
	{
		$this->_soBroadcast = $soBroadcast;
	}
	
	/**
	 * Merges this stream options with the given other options. This method
	 * does not modifies the current options object and creates a new object
	 * with the merged properties of both objects.
	 * 
	 * When merging, if two properties are defined within this object and the
	 * other, the properties of the other object will override the properties
	 * within current object.
	 * 
	 * @param ?NativeSocketOptions $other
	 * @return NativeSocketOptions
	 */
	public function mergeWith(?NativeSocketOptions $other) : NativeSocketOptions
	{
		if(null === $other)
		{
			return $this;
		}
		
		$newobj = new self();
		$newobj->_bindTo = $other->getBindTo() ?? $this->getBindTo();
		$newobj->_backlog = \max($other->getBacklog(), $this->getBacklog());
		$newobj->_ipv6Only = $other->shouldIpv6Only() || $this->shouldIpv6Only();
		$newobj->_soReuseport = $other->shouldSoReuseport() || $this->shouldSoReuseport();
		$newobj->_soBroadcast = $other->shouldSoBroadcast() || $this->shouldSoBroadcast();
		
		return $newobj;
	}
	
}

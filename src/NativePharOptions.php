<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-native library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Phar;
use Stringable;

/**
 * NativePharOptions class file.
 * 
 * This class represents all the options that are used for the phar stream
 * wrapper.
 * 
 * @author Anastaszor
 * @see https://secure.php.net/manual/en/context.phar.php
 */
class NativePharOptions implements Stringable
{
	
	/**
	 * One of Phar compression constants.
	 * 
	 * @var integer
	 * @see https://php.net/manual/en/phar.constants.php#phar.constants.compression
	 */
	protected int $_compress = Phar::NONE;
	
	/**
	 * Phar metadata. See Phar::setMetadata().
	 * 
	 * @var null|boolean|integer|float|string
	 * @see https://php.net/manual/en/phardata.setmetadata.php
	 */
	protected $_metadata;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the array that will be used to build the context.
	 *
	 * @return array<string, boolean|integer|float|string>
	 */
	public function toArray() : array
	{
		$opts = [
			'compress' => $this->_compress,
		];
		
		if(null !== $this->_metadata)
		{
			$opts['metadata'] = $this->_metadata;
		}
		
		return $opts;
	}
	
	/**
	 * Gets the compression mode.
	 * 
	 * @return integer
	 */
	public function getCompress() : int
	{
		return $this->_compress;
	}
	
	/**
	 * Gets the metadata of the resulted file.
	 * 
	 * @return null|boolean|integer|float|string
	 */
	public function getMetadata()
	{
		return $this->_metadata;
	}
	
	/**
	 * Sets the compression mode.
	 * 
	 * @param integer $compress
	 */
	public function setCompress(int $compress) : void
	{
		switch($compress)
		{
			case Phar::NONE:
			case Phar::COMPRESSED:
			case Phar::GZ:
			case Phar::BZ2:
				$this->_compress = $compress;
				break;
		}
	}
	
	/**
	 * Sets the metadata about the resulted file.
	 * 
	 * @param null|boolean|integer|float|string $metadata
	 */
	public function setMetadata($metadata) : void
	{
		$this->_metadata = $metadata;
	}
	
	/**
	 * Merges this stream options with the given other options. This method
	 * does not modifies the current options object and creates a new object
	 * with the merged properties of both objects.
	 *
	 * When merging, if two properties are defined within this object and the
	 * other, the properties of the other object will override the properties
	 * within current object.
	 *
	 * @param ?NativePharOptions $other
	 * @return NativePharOptions
	 */
	public function mergeWith(?NativePharOptions $other) : NativePharOptions
	{
		if(null === $other)
		{
			return $this;
		}
		
		$newobj = new self();
		$newobj->_compress = $other->getCompress();
		$newobj->_metadata = $other->getMetadata() ?? $this->getMetadata();
		
		return $newobj;
	}
	
}

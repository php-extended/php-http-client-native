<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-native library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Stringable;

/**
 * NativeMongodbOptions class file.
 * 
 * This class represents all the options that are used for the mongodb stream
 * wrapper.
 * 
 * @author Anastaszor
 * @see https://secure.php.net/manual/en/context.mongodb.php
 */
class NativeMongodbOptions implements Stringable
{
	
	/**
	 * A callback function called when inserting a document, see log_cmd_insert().
	 * 
	 * The function's signature should respect the following signature :
	 * log_cmd_insert ( array $server , array $document , array $writeOptions ,
	 * 		array $protocolOptions )
	 * 
	 * @var ?callable
	 * @phpstan-ignore-next-line
	 */
	protected $_logCmdInsert = null;
	
	/**
	 * A callback function called when deleting a document, see log_cmd_delete().
	 * 
	 * The function's signature should respect the following signature :
	 * log_cmd_delete ( array $server , array $writeOptions ,
	 * 		array $deleteOptions , array $protocolOptions )
	 * 
	 * @var ?callable
	 * @phpstan-ignore-next-line
	 */
	protected $_logCmdDelete = null;
	
	/**
	 * A callback function called when updating a document, see log_cmd_update().
	 * 
	 * The function's signature should respect the following signature :
	 * log_cmd_update ( array $server , array $writeOptions ,
	 * 		array $updateOptions , array $protocolOptions )
	 * 
	 * @var ?callable
	 * @phpstan-ignore-next-line
	 */
	protected $_logCmdUpdate = null;
	
	/**
	 * A callback function called when executing a Write Batch, see log_write_batch().
	 * 
	 * The function's signature should respect the following signature :
	 * log_write_batch ( array $server , array $writeOptions , array $batch ,
	 * 		array $protocolOptions )
	 * 
	 * @var ?callable
	 * @phpstan-ignore-next-line
	 */
	protected $_logWriteBatch = null;
	
	/**
	 * A callback function called when reading a reply from MongoDB, see log_reply().
	 * 
	 * The function's signature should respect the following signature :
	 * log_reply ( array $server , array $messageHeaders , array $operationHeaders )
	 * 
	 * @var ?callable
	 * @phpstan-ignore-next-line
	 */
	protected $_logReply = null;
	
	/**
	 * A callback function called when retrieving more results from a MongoDB
	 * cursor, see log_getmore().
	 * 
	 * The function's signature should respect the following signature :
	 * log_getmore ( array $server , array $info )
	 * 
	 * @var ?callable
	 * @phpstan-ignore-next-line
	 */
	protected $_logGetmore = null;
	
	/**
	 * A callback function called executing a killcursor opcode, see log_killcursor().
	 * 
	 * The function's signature should respect the following signature :
	 * log_killcursor ( array $server , array $info )
	 * 
	 * @var ?callable
	 * @phpstan-ignore-next-line
	 */
	protected $_logKillcursor = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the option values as array.
	 * 
	 * @return array<string, callable>
	 * @phpstan-ignore-next-line
	 */
	public function toArray() : array
	{
		$opts = [];
		
		if(null !== $this->_logCmdInsert)
		{
			$opts['log_cmd_insert'] = $this->_logCmdInsert;
		}
		
		if(null !== $this->_logCmdDelete)
		{
			$opts['log_cmd_delete'] = $this->_logCmdDelete;
		}
		
		if(null !== $this->_logCmdUpdate)
		{
			$opts['log_cmd_update'] = $this->_logCmdUpdate;
		}
		
		if(null !== $this->_logWriteBatch)
		{
			$opts['log_write_batch'] = $this->_logWriteBatch;
		}
		
		if(null !== $this->_logReply)
		{
			$opts['log_reply'] = $this->_logReply;
		}
		
		if(null !== $this->_logGetmore)
		{
			$opts['log_getmore'] = $this->_logGetmore;
		}
		
		if(null !== $this->_logKillcursor)
		{
			$opts['log_killcursor'] = $this->_logKillcursor;
		}
		
		return $opts;
	}
	
	/**
	 * Gets the callable to log insert commands.
	 * 
	 * @return ?callable
	 * @phpstan-ignore-next-line
	 */
	public function getLogCmdInsert() : ?callable
	{
		return $this->_logCmdInsert;
	}
	
	/**
	 * Gets the callable to log delete commands.
	 * 
	 * @return ?callable
	 * @phpstan-ignore-next-line
	 */
	public function getLogCmdDelete() : ?callable
	{
		return $this->_logCmdDelete;
	}
	
	/**
	 * Gets the callable to log the update commands.
	 * 
	 * @return ?callable
	 * @phpstan-ignore-next-line
	 */
	public function getLogCmdUpdate() : ?callable
	{
		return $this->_logCmdUpdate;
	}
	
	/**
	 * Gets the callable to log the write batches.
	 * 
	 * @return ?callable
	 * @phpstan-ignore-next-line
	 */
	public function getLogWriteBatch() : ?callable
	{
		return $this->_logWriteBatch;
	}
	
	/**
	 * Gets the callable to log the replies.
	 * 
	 * @return ?callable
	 * @phpstan-ignore-next-line
	 */
	public function getLogReply() : ?callable
	{
		return $this->_logReply;
	}
	
	/**
	 * Gets the callable to log the get more queries.
	 * 
	 * @return ?callable
	 * @phpstan-ignore-next-line
	 */
	public function getLogGetmore() : ?callable
	{
		return $this->_logGetmore;
	}
	
	/**
	 * Gets the callable to log the kill cursor queries.
	 * 
	 * @return ?callable
	 * @phpstan-ignore-next-line
	 */
	public function getLogKillcursor() : ?callable
	{
		return $this->_logKillcursor;
	}
	
	/**
	 * Sets the callable to log the insert commands.
	 * 
	 * @param callable $logCmdInsert
	 * @phpstan-ignore-next-line
	 */
	public function setLogCmdInsert(callable $logCmdInsert) : void
	{
		$this->_logCmdInsert = $logCmdInsert;
	}
	
	/**
	 * Sets the callable to log the delete commands.
	 * 
	 * @param callable $logCmdDelete
	 * @phpstan-ignore-next-line
	 */
	public function setLogCmdDelete(callable $logCmdDelete) : void
	{
		$this->_logCmdDelete = $logCmdDelete;
	}
	
	/**
	 * Sets the callable to log the update commands.
	 * 
	 * @param callable $logCmdUpdate
	 * @phpstan-ignore-next-line
	 */
	public function setLogCmdUpdate(callable $logCmdUpdate) : void
	{
		$this->_logCmdUpdate = $logCmdUpdate;
	}
	
	/**
	 * Sets the callable to log write batch queries.
	 * 
	 * @param callable $logWriteBatch
	 * @phpstan-ignore-next-line
	 */
	public function setLogWriteBatch(callable $logWriteBatch) : void
	{
		$this->_logWriteBatch = $logWriteBatch;
	}
	
	/**
	 * Sets the callable to log the reply queries.
	 * 
	 * @param callable $logReply
	 * @phpstan-ignore-next-line
	 */
	public function setLogReply(callable $logReply) : void
	{
		$this->_logReply = $logReply;
	}
	
	/**
	 * Sets the callable to log the get more queries.
	 * 
	 * @param callable $logGetmore
	 * @phpstan-ignore-next-line
	 */
	public function setLogGetmore(callable $logGetmore) : void
	{
		$this->_logGetmore = $logGetmore;
	}
	
	/**
	 * Sets the callable to log the kill cursor queries.
	 * 
	 * @param callable $logKillcursor
	 * @phpstan-ignore-next-line
	 */
	public function setLogKillcursor(callable $logKillcursor) : void
	{
		$this->_logKillcursor = $logKillcursor;
	}
	
	/**
	 * Merges this stream options with the given other options. This method
	 * does not modifies the current options object and creates a new object
	 * with the merged properties of both objects.
	 *
	 * When merging, if two properties are defined within this object and the
	 * other, the properties of the other object will override the properties
	 * within current object.
	 *
	 * @param ?NativeMongodbOptions $other
	 * @return NativeMongodbOptions
	 * @SuppressWarnings("PHPMD.UnusedLocalVariable")
	 */
	public function mergeWith(?NativeMongodbOptions $other) : NativeMongodbOptions
	{
		if(null === $other)
		{
			return $this;
		}
		
		$newobj = new self();
		
		$thisclb = $this->getLogCmdInsert() ?? function() : void {};
		$otherclb = $other->getLogCmdInsert() ?? function() : void {};
		$newobj->_logCmdInsert = function(array $server, array $document, array $writeOptions, array $protocolOptions) use ($thisclb, $otherclb) : void
		{
			/** @psalm-suppress TooManyArguments */
			$thisclb($server, $document, $writeOptions, $protocolOptions);
			/** @psalm-suppress TooManyArguments */
			$otherclb($server, $document, $writeOptions, $protocolOptions);
		};
		
		$thisclb = $this->getLogCmdDelete() ?? function() : void {};
		$otherclb = $other->getLogCmdDelete() ?? function() : void {};
		$newobj->_logCmdDelete = function(array $server, array $writeOptions, array $deleteOptions, array $protocolOptions) use ($thisclb, $otherclb) : void
		{
			/** @psalm-suppress TooManyArguments */
			$thisclb($server, $writeOptions, $deleteOptions, $protocolOptions);
			/** @psalm-suppress TooManyArguments */
			$otherclb($server, $writeOptions, $deleteOptions, $protocolOptions);
		};
		
		$thisclb = $this->getLogCmdUpdate() ?? function() : void {};
		$otherclb = $other->getLogCmdUpdate() ?? function() : void {};
		$newobj->_logCmdUpdate = function(array $server, array $writeOptions, array $updateOptions, array $protocolOptions) use ($thisclb, $otherclb) : void
		{
			/** @psalm-suppress TooManyArguments */
			$thisclb($server, $writeOptions, $updateOptions, $protocolOptions);
			/** @psalm-suppress TooManyArguments */
			$otherclb($server, $writeOptions, $updateOptions, $protocolOptions);
		};
		
		$thisclb = $this->getLogWriteBatch() ?? function() : void {};
		$otherclb = $other->getLogWriteBatch() ?? function() : void {};
		$newobj->_logWriteBatch = function(array $server, array $writeOptions, array $batch, array $protocolOptions) use ($thisclb, $otherclb) : void
		{
			/** @psalm-suppress TooManyArguments */
			$thisclb($server, $writeOptions, $batch, $protocolOptions);
			/** @psalm-suppress TooManyArguments */
			$otherclb($server, $writeOptions, $batch, $protocolOptions);
		};
		
		$thisclb = $this->getLogReply() ?? function() : void {};
		$otherclb = $other->getLogReply() ?? function() : void {};
		$newobj->_logReply = function(array $server, array $messageHeaders, array $operationHeaders) use ($thisclb, $otherclb) : void
		{
			/** @psalm-suppress TooManyArguments */
			$thisclb($server, $messageHeaders, $operationHeaders);
			/** @psalm-suppress TooManyArguments */
			$otherclb($server, $messageHeaders, $operationHeaders);
		};
		
		$thisclb = $this->getLogGetmore() ?? function() : void {};
		$otherclb = $other->getLogGetmore() ?? function() : void {};
		$newobj->_logGetmore = function(array $server, array $info) use ($thisclb, $otherclb) : void
		{
			/** @psalm-suppress TooManyArguments */
			$thisclb($server, $info);
			/** @psalm-suppress TooManyArguments */
			$otherclb($server, $info);
		};
		
		$thisclb = $this->getLogKillcursor() ?? function() : void {};
		$otherclb = $other->getLogKillcursor() ?? function() : void {};
		$newobj->_logKillcursor = function(array $server, array $info) use ($thisclb, $otherclb) : void
		{
			/** @psalm-suppress TooManyArguments */
			$thisclb($server, $info);
			/** @psalm-suppress TooManyArguments */
			$otherclb($server, $info);
		};
		
		return $newobj;
	}
	
}

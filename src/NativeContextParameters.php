<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-native library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Stringable;

/**
 * NativeContextParameters class file.
 * 
 * This class represents all the options that are used for the stream contexts.
 * 
 * @author Anastaszor
 * @see https://secure.php.net/manual/en/context.params.php
 */
class NativeContextParameters implements Stringable
{
	
	/**
	 * Ato be called when an event occurs on a stream.
	 * 
	 * @var ?callable
	 * @phpstan-ignore-next-line
	 */
	protected $_notification = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the notification callback. The signature of the function should
	 * respect : void stream_notification_callback ( 
	 * int $notification_code , int $severity , string $message , 
	 * int $message_code , int $bytes_transferred , int $bytes_max ).
	 * 
	 * @param callable $notification
	 * @phpstan-ignore-next-line
	 */
	public function setNotification(callable $notification) : void
	{
		$this->_notification = $notification;
	}
	
	/**
	 * Gets the callback of the notifications.
	 * 
	 * @return ?callable
	 * @phpstan-ignore-next-line
	 */
	public function getNotification() : ?callable
	{
		return $this->_notification;
	}
	
	/**
	 * Applies the context parameters to the given stream http context.
	 * 
	 * @param resource $context
	 */
	public function applyParamsToContext($context) : void
	{
		if(null === $this->_notification)
		{
			return;
		}
		
		/** @psalm-suppress UnusedFunctionCall */
		\stream_context_set_params($context, ['notification' => $this->_notification]);
	}
	
	/**
	 * Merges this stream options with the given other options. This method
	 * does not modifies the current options object and creates a new object
	 * with the merged properties of both objects.
	 *
	 * When merging, if two properties are defined within this object and the
	 * other, the properties of the other object will override the properties
	 * within current object.
	 *
	 * @param ?NativeContextParameters $other
	 * @return NativeContextParameters
	 */
	public function mergeWith(?NativeContextParameters $other) : NativeContextParameters
	{
		if(null === $other)
		{
			return $this;
		}
		
		$newobj = new self();
		
		$thisclb = $this->getNotification() ?? function() : void {};
		$otherclb = $other->getNotification() ?? function() : void {};
		$newobj->_notification = function(?int $notificationCode, ?int $severity, ?string $message, ?int $messageCode, ?int $bytesTransferred, ?int $bytesMax) use ($thisclb, $otherclb) : void
		{
			/** @psalm-suppress TooManyArguments */
			$thisclb($notificationCode, $severity, $message, $messageCode, $bytesTransferred, $bytesMax);
			/** @psalm-suppress TooManyArguments */
			$otherclb($notificationCode, $severity, $message, $messageCode, $bytesTransferred, $bytesMax);
		};
		
		return $newobj;
	}
	
}

# php-extended/php-http-client-native
A psr-18 compliant client with the native stream engine.

![coverage](https://gitlab.com/php-extended/php-http-client-native/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-http-client-native/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-http-client-native ^8`


## Basic Usage

This library uses the internal  `file_get_contents` with the http resource
context to perform http requests. You may use it the following way :

```php

use PhpExtended\HttpClient\NativeClient;
use PhpExtended\HttpClient\NativeOptionsFactory;

/** @var $request Psr\Http\Message\Request */ // psr-7
/** @var $responseFactory \Psr\Http\Client\ResponseFactoryInterface */ // psr-18
/** @var $streamFactory \Psr\Http\Client\StreamFactoryInterface */ // psr-18

$client = new NativeClient($responseFactory, $streamFactory, new NativeOptionsFactory());
$response = $client->sendRequest($request);

// $response is an instance of Psr\Http\Message\Response

```

To enforce https, you should use the `\PhpExtended\Certificate\CertificateProviderInterface` argument
of the client.



## License

MIT (See [license file](LICENSE)).
